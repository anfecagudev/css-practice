var backdrop = document.querySelector('.backdrop');
var modal = document.querySelector('.modal');
var modalNoButton = document.querySelector('.modal__action--negative');
var toggleButton = document.querySelector('.toggle-button');
var mobileNavv = document.querySelector('.mobile-nav');
var ctaButton = document.querySelector('.main-nav__item--cta');


var selectPlanButton = document.querySelectorAll('.plan button');


backdrop.addEventListener('click',function(){
    mobileNavv.classList.remove('open');
    closeModal();
});


for(var i = 0; i < selectPlanButton.length; i++){
    selectPlanButton[i].addEventListener('click', function(){
        // modal.style.display = 'block';
        // backdrop.style.display = 'block';
        // modal.className = 'open'   //this overwrite the complete style;
        modal.classList.add('open');
        backdrop.style.display = 'block';
        setTimeout(function(){
            backdrop.classList.add('open');
        },10);


    })
}

if(modalNoButton){
    modalNoButton.addEventListener('click', closeModal);
}



function closeModal(){
    // backdrop.style.display = 'none';
    // modal.style.display = 'none';
    backdrop.style.display = 'none';
    setTimeout(function(){
        backdrop.classList.remove('open');
    },10);
   
    if(modal){
        modal.classList.remove('open');
    }    
}

toggleButton.addEventListener('click', function(){
    mobileNavv.classList.add('open');
    backdrop.style.display = 'block';
    setTimeout(function(){
        backdrop.classList.add('open');
    },10);
    
});

ctaButton.addEventListener('animationstart', function(event){
    console.log('Animaiton Started', event)
});

ctaButton.addEventListener('animationend', function(event){
    console.log('Animaiton Ended', event)
});
ctaButton.addEventListener('animationiteration', function(event){
    console.log('Animaiton Iteration', event)
});